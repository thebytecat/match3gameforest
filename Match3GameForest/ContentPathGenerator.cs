﻿







  



namespace Match3GameForest
{
    class ContentPath
    {
		public static class Fonts
		{
			public static string comics_font = @"Fonts\comics_font";
			public static string comics_font_0 = @"Fonts\comics_font_0";
			public static string score_font = @"Fonts\score_font";
			public static string score_font_0 = @"Fonts\score_font_0";
			public static string small_font = @"Fonts\small_font";
			public static string small_font_0 = @"Fonts\small_font_0";
		}

		public static string arrow = @"arrow";
		public static string bg = @"bg";
		public static string bg_effect_flare = @"bg_effect_flare";
		public static string candy = @"candy";
		public static string candy2 = @"candy2";
		public static string explosion = @"explosion";
		public static string explosion_3 = @"explosion_3";
		public static string gameBG = @"gameBG";
		public static string tiles = @"tiles";


    }
}



