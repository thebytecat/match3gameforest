﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using MonoGame.Extended.Screens;
using MonoGame.Extended.ViewportAdapters;

namespace Match3GameForest.Screens
{
    public class BaseScreen : Screen
    {
        protected GameWindow Window => Game.Window;
        protected GraphicsDevice GraphicsDevice => Game.GraphicsDevice;
        protected GameComponentCollection Components => Game.Components;
        protected ContentManager Content => Game.Content;
        protected Camera2D Camera { get; private set; }
        protected GameMain Game { get; }

        protected BaseScreen(GameMain game)
        {
            Game = game;

            Window.AllowUserResizing = false;
        }

        public override void LoadContent()
        {
            base.LoadContent();
            Camera = Game.Services.GetService<Camera2D>();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }
}
