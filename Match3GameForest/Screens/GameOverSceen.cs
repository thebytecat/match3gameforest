﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended;
using MonoGame.Extended.BitmapFonts;

namespace Match3GameForest.Screens
{
    public class GameOverSceen : BaseScreen
    {
        private SpriteBatch spriteBatch;
        private Texture2D backgroundTexture;
        private BitmapFont font;
        private MenuItem okMenuItem;
        private Vector2 gameOverPosition;

        public GameOverSceen(GameMain game) : base(game) { }

        public override void LoadContent()
        {
            base.LoadContent();

            spriteBatch = Game.Services.GetService<SpriteBatch>();

            backgroundTexture = Content.Load<Texture2D>(ContentPath.gameBG);
            font = Content.Load<BitmapFont>(ContentPath.Fonts.score_font);
            okMenuItem = new MenuItem(font, "OK") {
                Position = Camera.BoundingRectangle.Center - (font.MeasureString("OK") / 2),
                Action = Show<MenuScreen>
            };
            gameOverPosition = Camera.BoundingRectangle.Center - (font.MeasureString("GAME OVER") / 2) - new Vector2(0, 100);
        }

        private MouseState previousState;

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            var mouseState = Mouse.GetState();
            var isPressed = mouseState.LeftButton == ButtonState.Released && previousState.LeftButton == ButtonState.Pressed;

            var isHovered = okMenuItem.BoundingRectangle.Contains(mouseState.Position);

            okMenuItem.Color = isHovered ? Color.LightSlateGray : Color.DarkSlateGray;

            if (isHovered && isPressed) {
                okMenuItem.Action?.Invoke();
            }

            previousState = mouseState;
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            spriteBatch.Begin(transformMatrix: Camera.GetViewMatrix());

            spriteBatch.Draw(backgroundTexture, new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), Color.White);
            spriteBatch.DrawString(font, "GAME OVER", gameOverPosition, Color.DarkSlateGray);
            okMenuItem.Draw(spriteBatch);

            spriteBatch.End();
        }

    }
}