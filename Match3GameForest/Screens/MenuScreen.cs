﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.Screens;
using MonoGame.Extended;
using MonoGame.Extended.BitmapFonts;
using MonoGame.Extended.Gui;
using MonoGame.Extended.ViewportAdapters;

namespace Match3GameForest.Screens
{
    public class MenuScreen : BaseScreen
    {
        private SpriteBatch spriteBatch;
        private Texture2D backgroundTexture;


        public MenuScreen(GameMain game) : base(game)
        {
            MenuItems = new List<MenuItem>();
        }

        public List<MenuItem> MenuItems { get; }
        protected BitmapFont Font { get; private set; }

        protected void AddMenuItem(string text, Action action)
        {
            var menuItem = new MenuItem(Font, text) {
                Position = Camera.BoundingRectangle.Center - (Font.MeasureString(text) / 2) + new Vector2(0, MenuItems.Count * 50),
                Action = action
            };

            MenuItems.Add(menuItem);
        }

        public override void Dispose()
        {
            base.Dispose();

            spriteBatch.Dispose();
        }

        public override void LoadContent()
        {
            base.LoadContent();

            spriteBatch = new SpriteBatch(GraphicsDevice);

            backgroundTexture = Content.Load<Texture2D>(ContentPath.gameBG);
            Font = Content.Load<BitmapFont>(ContentPath.Fonts.score_font);

            AddMenuItem("Play", Show<GameScreen>);
        }

        public override void UnloadContent()
        {
            Content.Unload();
            Content.Dispose();

            base.UnloadContent();
        }

        private MouseState previousState;

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            var mouseState = Mouse.GetState();
            var isReleased = mouseState.LeftButton == ButtonState.Released && previousState.LeftButton == ButtonState.Pressed;
           
            foreach (var menuItem in MenuItems) {
                var isHovered = menuItem.BoundingRectangle.Contains(mouseState.Position);

                menuItem.Color = isHovered ? Color.LightSlateGray : Color.DarkSlateGray;

                if (isHovered && isReleased) {
                    menuItem.Action?.Invoke();
                    break;
                }
            }

            previousState = mouseState;
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            spriteBatch.Begin(transformMatrix: Camera.GetViewMatrix());

            spriteBatch.Draw(backgroundTexture, new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), Color.White);

            foreach (var menuItem in MenuItems)
                menuItem.Draw(spriteBatch);

            spriteBatch.End();
        }
    }
}
