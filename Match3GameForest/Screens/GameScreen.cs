﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Match3GameForest.Components;
using Match3GameForest.Entities;
using Match3GameForest.Entities.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.BitmapFonts;
using MonoGame.Extended.Entities;
using MonoGame.Extended.Screens;

namespace Match3GameForest.Screens
{
    class GameScreen : BaseScreen
    {
        private EntityComponentSystem ecs;
        private EntityFactory entityFactory;
        private Board board;
        private SpriteBatch spriteBatch;
        private Texture2D backgroundTexture;
        private BitmapFont font;

        private void DestroyBoard()
        {
            var pieces = ecs.EntityManager.GetEntitiesByGroup("PIECE");

            foreach (var entity in pieces) {
                entity.Destroy();
            }

            var entities = ecs.EntityManager.GetEntitiesByGroup("BOARD");
            foreach (var entity in entities) {
                entity.Destroy();
            }

            board = null;
        }

        public GameScreen(GameMain game) : base(game)
        {

        }
        
        public void GoToMAinScreen()
        {
            DestroyBoard();

            Show<MenuScreen>();
        }

        public void GameOver()
        {
            DestroyBoard();
            Show<GameOverSceen>();
        }

        public override void LoadContent()
        {
            base.LoadContent();
            spriteBatch = Game.Services.GetService<SpriteBatch>();
            ecs = Game.Services.GetService<EntityComponentSystem>();
            entityFactory = new EntityFactory(ecs, Content);
            
            backgroundTexture = Game.Content.Load<Texture2D>(ContentPath.gameBG);
            font = Content.Load<BitmapFont>(ContentPath.Fonts.small_font);
        }

        public override void UnloadContent()
        {
            backgroundTexture.Dispose();
            DestroyBoard();

            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (board == null) {
                ecs.EntityManager.CreateEntity();
                board = entityFactory.CreateBoard().Get<Board>();
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Escape)) {
                GoToMAinScreen();
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin(transformMatrix: Camera.GetViewMatrix());
            spriteBatch.Draw(backgroundTexture, new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), Color.White);
#if DEBUG
            var debugStr = $"Active entity count: {ecs.EntityManager.ActiveEntitiesCount} Tolal count: {ecs.EntityManager.TotalEntitiesCount} \nBoard state: {board.State}";
            spriteBatch.DrawString(font, debugStr, new Vector2(16, 16), Color.White);
#endif
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
