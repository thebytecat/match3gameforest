﻿using System;
using System.Reflection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended.Screens;
using MonoGame.Extended;
using MonoGame.Extended.Entities;
using Match3GameForest.Screens;
using MonoGame.Extended.Animations;
using MonoGame.Extended.BitmapFonts;
using MonoGame.Extended.Tweening;
using MonoGame.Extended.ViewportAdapters;

namespace Match3GameForest
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameMain : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private ScreenGameComponent screenGameComponent;

        private EntityComponentSystem ecs;
        private FramesPerSecondCounter fpsCounter = new FramesPerSecondCounter();

        public ViewportAdapter ViewportAdapter { get; private set; }

        public GameMain()
        {
            graphics = new GraphicsDeviceManager(this) {
                IsFullScreen = false,
                PreferredBackBufferWidth = 640,
                PreferredBackBufferHeight = 960
            };

            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            Window.AllowUserResizing = false;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            ecs = new EntityComponentSystem(this);

            // scan for components and systems in provided assemblies
            ecs.Scan(Assembly.GetExecutingAssembly());

            Services.AddService(ecs);

            base.Initialize();
            screenGameComponent = new ScreenGameComponent(this);

            screenGameComponent.Register(new MenuScreen(this));
            screenGameComponent.Register(new GameScreen(this));
            screenGameComponent.Register(new GameOverSceen(this));
            Components.Add(screenGameComponent);
            Services.AddService(screenGameComponent);
            screenGameComponent.Initialize();

            var animationComponent = new AnimationComponent(this);
            var tweenComponent = new TweeningComponent(this, animationComponent);
            Components.Add(animationComponent);
            Components.Add(tweenComponent);
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            ViewportAdapter = new BoxingViewportAdapter(Window, GraphicsDevice, 640, 960);
            var camera = new Camera2D(ViewportAdapter);

            Services.AddService(camera);
            Services.AddService(ViewportAdapter);
            Services.AddService(spriteBatch);

            base.LoadContent();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            fpsCounter.Update(gameTime);
            Window.Title = $"Match3  FPS:{fpsCounter.FramesPerSecond}";

            base.Update(gameTime);

            ecs.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            fpsCounter.Draw(gameTime);
            
            base.Draw(gameTime);

            ecs.Draw(gameTime);
        }
    }
}
