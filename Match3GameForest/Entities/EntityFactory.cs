﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Match3GameForest.Components;
using Match3GameForest.Entities.Components;
using Match3GameForest.Entities.Systems;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using MonoGame.Extended.Entities;
using MonoGame.Extended.TextureAtlases;

namespace Match3GameForest.Entities
{
    public class EntityFactory
    {
        private readonly EntityComponentSystem ecs;
        private readonly EntityManager entityManager;
        private readonly FastRandom random;
        private int tileSpriteSize = 140;

        public EntityFactory(EntityComponentSystem ecs, ContentManager contentManager)
        {
            this.ecs = ecs;
            entityManager = ecs.EntityManager;
            var rand = new Random();
            random = new FastRandom(rand.Next());

            LoadContent(contentManager);
        }

        private void LoadContent(ContentManager content)
        {

        }

        public Entity CreateBoard()
        {
            var entity = ecs.EntityManager.CreateEntity();

            entity.Group = "BOARD";
            entity.Name = "Board";

            var board = entity.Attach<Board>();
            board.CellSize = 64;
            board.PieceTypeCount = 5;

            var transform = entity.Attach<TransformComponent>();
            transform.Position = new Vector2(ecs.GraphicsDevice.Viewport.Bounds.Center.X - (board.BoardSize.X * board.CellSize / 2), 200);

            var size = board.BoardSize.ToVector2() * board.CellSize;
            board.Bounds = new RectangleF(transform.Position, size);

            for (int x = 0; x < board.BoardSize.X; x++) {
                for (int y = 0; y < board.BoardSize.Y; y++) {
                    var piece = CreatePiece(board, x, y).Get<BoardPiece>();
                    board[x, y] = piece;
                }
            }

            entity.Attach<BoardInputComponent>();

            var swap = entity.Attach<PieceSwapComponent>();
            swap.SwapDuration = 1f;

            entity.Attach<BoardFillComponent>();
            entity.Attach<PieceMatchingComponent>();
            entity.Attach<BonusMatchingComponent>();
            entity.Attach<ScoreCountComponent>();

            var levelTime = entity.Attach<LevelTimeComponent>();
            levelTime.TimeToLevel = 60;

            return entity;
        }

        public Entity CreatePiece(Board board, Point cell)
        {
            return CreatePiece(board, cell.X, cell.Y);
        }

        public Entity CreatePiece(Board board, int x, int y)
        {
            var entity = entityManager.CreateEntity();
            entity.Group = "PIECE";
            var transform = entity.Attach<TransformComponent>();
            transform.Scale = new Vector2((float)board.CellSize / tileSpriteSize, (float)board.CellSize / tileSpriteSize);
            transform.Position = board.CellToWorldPosition(new Point(x, y));

            var piece = entity.Attach<BoardPiece>();
            piece.Type = (PieceType)random.Next(board.PieceTypeCount - 1);

            return entity;
        }

        public Entity CreateDestroyer()
        {
            var entity = entityManager.CreateEntity();
            entity.Group = "BOARD";
            entity.Attach<TransformComponent>();
            entity.Attach<SpriteComponent>();

            return entity;
        }

        public Entity CreateExplotion(float duration, Vector2 position, int cellSize)
        {
            var entity = entityManager.CreateEntity();
            var sprite = entity.Attach<SpriteComponent>();
            var transform = entity.Attach<TransformComponent>();
            transform.Position = position;
            var explosion = entity.Attach<ExplosionComponent>();
            explosion.Duration = duration;
            explosion.CellSize = cellSize;

            return entity;
        }
    }
}
