﻿using System;
using Match3GameForest.Components;
using Microsoft.Xna.Framework;
using MonoGame.Extended;
using MonoGame.Extended.Entities;
using MonoGame.Extended.Tweening;

namespace Match3GameForest.Entities.Components
{
    [EntityComponent]
    public class PieceSwapComponent : EntityComponent
    {
        public float SwapDuration { get; set; }
        public Point CellFrom { get; set; }
        public Point CellTo { get; set; }

        public void Swap(Point from, Point to, Board board)
        {
            CellFrom = from;
            CellTo = to;
            var positionFrom = board.CellToWorldPosition(CellFrom);
            var positionTo = board.CellToWorldPosition(CellTo);

            var pieceA = board.GetPiece(CellFrom);
            var pieceB = board.GetPiece(CellTo);

            var transformA = pieceA.Entity.Get<TransformComponent>();
            transformA.CreateTweenGroup(() => {
                board[CellTo.X, CellTo.Y] = pieceA;
            })
                .MoveTo(positionTo, SwapDuration, EasingFunctions.ElasticInOut);

            var transformB = pieceB.Entity.Get<TransformComponent>();
            transformB.CreateTweenGroup(() => {
                board[CellFrom.X, CellFrom.Y] = pieceB;
            }).MoveTo(positionFrom, SwapDuration, EasingFunctions.ElasticInOut);
        }

        public void PlayNotAvailableSwapAnimation(Point from, Point to, Board board)
        {
            var pieceA = board.GetPiece(from);
            var pieceB = board.GetPiece(to);

            var transformA = pieceA.Entity.Get<TransformComponent>();
            var transformB = pieceB.Entity.Get<TransformComponent>();

            var posA = board.CellToWorldPosition(from);
            var posB = board.CellToWorldPosition(to);

            var AToB = (transformB.Position - transformA.Position).NormalizedCopy();
            var BToA = (transformA.Position - transformB.Position).NormalizedCopy();
            transformA.CreateTweenChain().Move(AToB * board.CellSize / 6, 0.5f, EasingFunctions.ElasticOut)
                .MoveTo(posA, 0.3f, EasingFunctions.Linear);

            transformB.CreateTweenChain().Move(BToA * board.CellSize / 6, 0.5f, EasingFunctions.ElasticOut)
                .MoveTo(posB, 0.3f, EasingFunctions.Linear);
            
        }

    }
}