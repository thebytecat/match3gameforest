﻿using System;
using MonoGame.Extended.Entities;

namespace Match3GameForest.Entities.Components
{
    [EntityComponent]
    public class LevelTimeComponent : EntityComponent
    {
        public int TimeToLevel { get; set; }
        public TimeSpan TimeLeft { get; set; }

        public override void Reset()
        {
            base.Reset();
            TimeToLevel = 0;
            TimeLeft = TimeSpan.Zero;
        }
    }
}