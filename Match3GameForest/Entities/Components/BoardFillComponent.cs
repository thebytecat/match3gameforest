﻿using System.Linq;
using Match3GameForest.Components;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Entities;

namespace Match3GameForest.Entities.Components
{
    [EntityComponent]
    public class BoardFillComponent : EntityComponent
    {
        public int[] EmptyCellInColumn { get; set; }
        public int FallingCellCount { get; set; }

        //falling speed in sec per cell
        public float FallingSpeed { get; set; } = 0.25f;

        public bool HaveEmptyCell => EmptyCellInColumn.Any(t => t > 0);

        public void CellValueChangeHandler(Point cell, BoardPiece oldValue, BoardPiece newValue)
        {
            if (newValue == null) {
                EmptyCellInColumn[cell.X]++;
            }
        }

        public override void Reset()
        {
            base.Reset();
            FallingCellCount = 0;
        }
    }
}