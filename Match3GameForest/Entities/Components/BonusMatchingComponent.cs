﻿using System;
using System.Collections.Generic;
using System.Linq;
using Match3GameForest.Components;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Entities;

namespace Match3GameForest.Entities.Components
{
    [EntityComponent]
    public class BonusMatchingComponent : EntityComponent
    {

        public List<Tuple<Point, BonusType>> MatchedBonuses { get; } = new List<Tuple<Point, BonusType>>();
        public List<Match> Matches { get; set; }
        public int ActiveBonuses { get; set; }
        

        public void HandleMatch(List<Match> matches, Board board)
        {
            Matches = matches;
            foreach (var match in matches) {
                foreach (var matchedCell in match.MatchedCells) {
                    var piece = board[matchedCell.X, matchedCell.Y];
                    if (piece.Bonus != BonusType.None) {
                        MatchedBonuses.Add(new Tuple<Point, BonusType>(matchedCell, piece.Bonus));
                    }
                }
            }
        }

        public override void Reset()
        {
            base.Reset();
            MatchedBonuses.Clear();
            Matches = null;
            ActiveBonuses = 0;
        }
    }
}