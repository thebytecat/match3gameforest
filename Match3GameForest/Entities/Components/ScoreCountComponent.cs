﻿using MonoGame.Extended.Entities;

namespace Match3GameForest.Entities.Components
{
    [EntityComponent]
    public class ScoreCountComponent : EntityComponent
    {
        public int Score { get; set; }

        public override void Reset()
        {
            base.Reset();
            Score = 0;
        }
    }
}