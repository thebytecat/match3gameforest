﻿using System.Collections.Generic;
using System.Linq;
using Match3GameForest.Components;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Collections;
using MonoGame.Extended.Entities;

namespace Match3GameForest.Entities.Components
{
    public class Match
    {
        public PieceType Type { get; set; }
        public Point CoreCell { get; set; }
        public List<Point> MatchedCells { get; } = new List<Point>(6);
    }

    [EntityComponent]
    public class PieceMatchingComponent : EntityComponent
    {
        public Board Board { get; set; }
        public List<Point> ChangedCells { get; } = new List<Point>();

        private readonly Vector2[][] possibleMatchs;

        public PieceMatchingComponent()
        {
            var up = new Vector2(0, -1);
            var down = new Vector2(0, 1);
            var left = new Vector2(-1, 0);
            var right = new Vector2(1, 0);

            possibleMatchs = new[] {
                new[] {up, down },
                new[] {left, right},
                new[] {up, up * 2},
                new[] {down, down * 2},
                new[] {left, left * 2},
                new[] {right, right * 2}
            };
        }

        private bool CheckMatchInCell(Point cell)
        {
            var piece = Board.GetPiece(cell);
            if (piece == null)
                return false;
            var pieceType = piece.Type;

            return possibleMatchs.Any(line =>
            {
                return line.All(c =>
                {
                    var p = Board.GetPiece(cell + c.ToPoint());
                    return p != null && p.Type == pieceType;
                });
            });
        }
       
        private List<Match> GetMatchesInLine(IReadOnlyList<Point> line)
        {
            var matches = new List<Match>();

            if (line.Count == 0)
                return matches;

            var tempCells = new List<Point>(5) { line[0] };
            for (var i = 1; i < line.Count; i++) {
                var currentCell = line[i];
                if (Board.GetPiece(currentCell).Type == Board.GetPiece(tempCells[0]).Type) {
                    tempCells.Add(currentCell);
                } else {
                    if (tempCells.Count >= 3) {
                        var match = new Match { Type = Board.GetPiece(tempCells[0]).Type };
                        match.MatchedCells.AddRange(tempCells);
                        matches.Add(match);
                    }
                    tempCells.Clear();
                    tempCells.Add(currentCell);
                }
            }

            if (tempCells.Count >= 3) {
                var match = new Match { Type = Board.GetPiece(tempCells[0]).Type };
                match.MatchedCells.AddRange(tempCells);
                matches.Add(match);
            }

            return matches;
        }

        private List<Match> CombineLineMatches(List<Match> vMatches, List<Match> hMatches)
        {
            var result = new List<Match>();
            foreach (var match in vMatches) {
                var crossMatches = hMatches.Where(m => m.MatchedCells.Intersect(match.MatchedCells).Any()).ToArray();
                if (crossMatches.Any()) {
                    foreach (var m in crossMatches) {
                        match.MatchedCells.AddRange(m.MatchedCells.Except(match.MatchedCells));
                        hMatches.Remove(m);
                    }
                }
                result.Add(match);
            }

            result.AddRange(hMatches);

            FindCoreCells(result);

            return result;
        }

        private void FindCoreCells(List<Match> matches)
        {
            foreach (var match in matches) {
                match.CoreCell = ChangedCells.FindLast(cell => match.MatchedCells.Contains(cell));
            }
        }

        public List<Match> GetAllMatches()
        {
            var vMatches = new List<Match>();
            for (int x = 0; x < Board.BoardSize.X; x++) {
                vMatches.AddRange(GetMatchesInLine(Enumerable.Range(0, Board.BoardSize.Y).Select(y => new Point(x, y)).ToList()));
            }

            var hMatches = new List<Match>();
            for (int y = 0; y < Board.BoardSize.Y; y++) {
                hMatches.AddRange(GetMatchesInLine(Enumerable.Range(0, Board.BoardSize.X).Select(x => new Point(x, y)).ToList()));
            }

            return CombineLineMatches(vMatches, hMatches);
        }

        public void CellValueChangeHandler(Point cell, BoardPiece oldValue, BoardPiece newValue)
        {
            if (newValue != null) {
                ChangedCells.Add(cell);
            }
        }

        public bool CheckSwap(Point cellFrom, Point cellTo)
        {
            var pieceA = Board.GetPiece(cellFrom);
            var pieceB = Board.GetPiece(cellTo);

            if (pieceA == null || pieceB == null)
                return false;

            var temp = pieceA.Type;
            pieceA.Type = pieceB.Type;
            pieceB.Type = temp;

            var checkResult = CheckMatchInCell(cellFrom) || CheckMatchInCell(cellTo);

            pieceB.Type = pieceA.Type;
            pieceA.Type = temp;

            return checkResult;
        }

        public override void Reset()
        {
            base.Reset();
            ChangedCells.Clear();
        }
    }
}