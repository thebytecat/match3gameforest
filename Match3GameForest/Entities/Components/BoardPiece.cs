﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Match3GameForest.Entities.Components;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Entities;
using MonoGame.Extended.Tweening;

namespace Match3GameForest.Components
{
    [EntityComponent]
    [EntityComponentPool(InitialSize = 64)]
    public class BoardPiece : EntityComponent
    {
        private bool isSelected = false;
        private TweenAnimation<TransformComponent> selectedTween;
        private TransformComponent transform;

        public PieceType Type { get; set; }
        public BonusType Bonus { get; set; }

        public bool IsSelected
        {
            get => isSelected;
            set
            {
                isSelected = value;
                if (isSelected) {
                    CreateSelectedTween();
                }
            }
        }

        private void CreateSelectedTween()
        {
            transform = Entity.Get<TransformComponent>();
            RotateLeft();
        }

        private void RotateLeft()
        {
            if (IsSelected) {
                transform.CreateTweenChain(RotateRight)
                    .RotateTo(MathHelper.Pi / 8, 0.5f, EasingFunctions.Linear);
            } else {
                transform.CreateTweenChain()
                    .RotateTo(0, 0.5f, EasingFunctions.Linear);
            }

        }

        private void RotateRight()
        {
            if (IsSelected) {
                transform.CreateTweenChain(RotateLeft)
                    .RotateTo(-MathHelper.Pi / 8, 0.5f, EasingFunctions.Linear);
            } else {
                transform.CreateTweenChain()
                    .RotateTo(0, 0.5f, EasingFunctions.Linear);
            }
        }

        private void StopSelectedTween()
        {
            if (selectedTween != null) {
                selectedTween.RotateTo(0, 0.3f, EasingFunctions.Linear);
                selectedTween = null;
            }
        }

        public override void Reset()
        {
            base.Reset();
            Bonus = BonusType.None;
            selectedTween = null;
        }
    }
}
