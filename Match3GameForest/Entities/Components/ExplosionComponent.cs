﻿using MonoGame.Extended.Entities;

namespace Match3GameForest.Entities.Components
{
    [EntityComponent]
    public class ExplosionComponent : EntityComponent
    {
        public float Duration { get; set; }
        public int CellSize { get; set; }
    }
}