﻿using Match3GameForest.Components;
using Microsoft.Xna.Framework;
using MonoGame.Extended;
using MonoGame.Extended.Collections;
using MonoGame.Extended.Entities;

namespace Match3GameForest.Entities.Components
{
    public enum PieceType { None = -1, Blue, Green, Orange, Purple, Red, Yellow}
    public enum BonusType { None, HorizontalLine, VerticalLine, Bomb}
    public enum BoardState { Playing, Swapping, Falling, Matching }
    

    [EntityComponent]
    //[EntityComponentPool(InitialSize = 1, IsFullPolicy = ObjectPoolIsFullPolicy.KillExisting)]
    public class Board : EntityComponent
    {
        private BoardPiece[,] cells;
        private Point boardSize = new Point(8);

        public int CellSize { get; set; }
        public RectangleF Bounds { get; set; }
        public int PieceTypeCount { get; set; }
        public BoardState State { get; set; }
        

        public delegate void OnCellValueChangeDelegate(Point cell, BoardPiece oldValue, BoardPiece newValue);
        public event OnCellValueChangeDelegate OnCellValueChange;

        public Point BoardSize
        {
            get => boardSize;
            set
            {
                if (boardSize != value) {
                    cells = new BoardPiece[value.X, value.Y];
                }
                boardSize = value;
            }
        }

        public Board()
        {
            cells = new BoardPiece[BoardSize.X, BoardSize.Y];
        }

        public BoardPiece GetPiece(Point cell)
        {
            if (cell.X >= 0 && cell.X < BoardSize.X && cell.Y >= 0 && cell.Y < BoardSize.Y) {
                var piece = cells[cell.X, cell.Y];
                return piece;
            }

            return null;
        }

        public Vector2 CellToWorldPosition(Point cell)
        {
            return Bounds.TopLeft + (cell.ToVector2() * CellSize) + Vector2.One * CellSize / 2f;
        }

        public BoardPiece this[int x, int y]
        {
            get {
                if (x >= 0 && x < BoardSize.X && y >= 0 && y < BoardSize.Y) {
                    return cells[x, y];
                }

                return null;
            }
            set
            {
                var piece = cells[x, y];
                OnCellValueChange?.Invoke(new Point(x,y), piece, value);
                if (value == null) {
                    piece?.Entity.Destroy();
                }
                cells[x, y] = value;
            }
        }


        public override void Reset()
        {
            base.Reset();
            State = BoardState.Playing;
            OnCellValueChange = null;
        }
    }
}
