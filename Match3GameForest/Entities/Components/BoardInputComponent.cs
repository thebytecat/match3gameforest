﻿using Match3GameForest.Components;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Entities;

namespace Match3GameForest.Entities.Components
{
    [EntityComponent]
    public class BoardInputComponent : EntityComponent
    {
        public Point? SelectedCell { get; set; }

        public override void Reset()
        {
            base.Reset();
            SelectedCell = null;
        }
    }
}