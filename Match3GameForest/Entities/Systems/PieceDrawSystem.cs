﻿using System;
using Match3GameForest.Components;
using Match3GameForest.Entities.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using MonoGame.Extended.Entities;
using MonoGame.Extended.TextureAtlases;

namespace Match3GameForest.Entities.Systems
{
    [Aspect(AspectType.All, typeof(BoardPiece), typeof(TransformComponent))]
    [EntitySystem(GameLoopType.Draw, Layer = 0)]
    public class PieceDrawSystem : EntityProcessingSystem
    {
        private Camera2D camera;
        private SpriteBatch spriteBatch;
        private TextureAtlas PieceTileAtlas { get; set; }


        public override void Initialize()
        {
            base.Initialize();
            camera = Game.Services.GetService<Camera2D>();
            spriteBatch = Game.Services.GetService<SpriteBatch>();
        }

        public override void LoadContent()
        {
            base.LoadContent();
            var tileAtlas = Game.Content.Load<Texture2D>(ContentPath.candy2);
            PieceTileAtlas = TextureAtlas.Create("tile_texture_atlas", tileAtlas, 128, 128);
        }

        protected override void Begin(GameTime gameTime)
        {
            var transformMatrix = camera.GetViewMatrix();

            spriteBatch.Begin(transformMatrix: transformMatrix);
        }

        protected override void Process(GameTime gameTime, Entity entity)
        {
            var piece = entity.Get<BoardPiece>();
            var transform = entity.Get<TransformComponent>();
            var textureIdx = (int)piece.Type;
            if (piece.Bonus == BonusType.Bomb) {
                textureIdx += 15;
            }
            else {
                textureIdx = (textureIdx * 3) + (int)piece.Bonus;
            }
            var textureRegion = PieceTileAtlas[textureIdx];
            var scale = transform.WorldScale;
            var origin = new Vector2(textureRegion.Width / 2f, textureRegion.Height / 2f);
            spriteBatch.Draw(textureRegion, transform.Position, Color.White, transform.Rotation, origin, scale, SpriteEffects.None, 0);
        }

        protected override void End(GameTime gameTime)
        {
            spriteBatch.End();
        }
    }
}