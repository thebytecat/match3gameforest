﻿using System;
using System.Collections.Generic;
using System.Linq;
using Match3GameForest.Components;
using Match3GameForest.Entities.Components;
using MonoGame.Extended;
using MonoGame.Extended.Entities;

namespace Match3GameForest.Entities.Systems
{
    [Aspect(AspectType.All, typeof(Board))]
    [EntitySystem(GameLoopType.Update, Layer = 0)]
    public class BoardGeneratingSystem : EntityProcessingSystem
    {
        private FastRandom random;

        private PieceType GetRandomElementExclude(IEnumerable<PieceType> exclude, int tileCount)
        {
            var excludeCollection = Enumerable.Range(0, tileCount).Where(x => !exclude.Contains((PieceType)x)).ToArray();
            int idx = random.Next(0, excludeCollection.Length - 1);
            return (PieceType)excludeCollection[idx];
        }

        public override void Initialize()
        {
            base.Initialize();
            var rand = new Random();
            random = new FastRandom(rand.Next());
        }

        public override void OnEntityAdded(Entity entity)
        {
            base.OnEntityAdded(entity);
            var board = entity.Get<Board>();

            var takenElements = new List<PieceType>(2);
            for (var x = 0; x < board.BoardSize.X; x++) {
                for (var y = 0; y < board.BoardSize.Y; y++) {
                    takenElements.Clear();
                    if (y > 1 && board[x, y - 1].Type == board[x, y - 2].Type)
                        takenElements.Add(board[x, y - 1].Type);
                    if (x > 1 && board[x - 1, y].Type == board[x - 2, y].Type)
                        takenElements.Add(board[x - 1, y].Type);
                    if (takenElements.Count > 0 && takenElements.Contains(board[x, y].Type)) {
                        board[x, y].Type = GetRandomElementExclude(takenElements, board.PieceTypeCount);
                    }
                }
            }
        }
    }
}