﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Match3GameForest.Components;
using Match3GameForest.Entities.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using MonoGame.Extended.Entities;
using MonoGame.Extended.Graphics;

namespace Match3GameForest.Entities.Systems
{
    [Aspect(AspectType.All, typeof(Board), typeof(TransformComponent))]
    [EntitySystem(GameLoopType.Draw, Layer = 0)]
    public class BoardDrawSystem : EntityProcessingSystem
    {
        private Camera2D camera;
        private SpriteBatch spriteBatch;
        private Color evenCellColor;
        private Color oddCellColor;

        public override void Initialize()
        {
            base.Initialize();

            camera = Game.Services.GetService<Camera2D>();
            spriteBatch = Game.Services.GetService<SpriteBatch>();

            evenCellColor = Color.LightSlateGray;
            oddCellColor = Color.DarkSlateGray;
        }

        public override void LoadContent()
        {
            base.LoadContent();
        }

        protected override void Begin(GameTime gameTime)
        {
            var transformMatrix = camera.GetViewMatrix();

            spriteBatch.Begin(transformMatrix: transformMatrix);
        }

        protected override void Process(GameTime gameTime, Entity entity)
        {
            var board = entity.Get<Board>();
            var size = new Size2(board.CellSize, board.CellSize);
            spriteBatch.DrawRectangle(board.Bounds, Color.Black);
            for (int x = 0; x < board.BoardSize.X; x++) {
                for (int y = 0; y < board.BoardSize.Y; y++) {
                    var color = (x + y) % 2 == 0 ? oddCellColor : evenCellColor;
                    var position = board.Bounds.TopLeft + new Vector2(x, y) * board.CellSize;
                    spriteBatch.FillRectangle(position, size, color);
                }
            }
        }

        protected override void End(GameTime gameTime)
        {
            spriteBatch.End();
        }
    }
}
