﻿using System.Linq;
using Match3GameForest.Entities.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Animations.SpriteSheets;
using MonoGame.Extended.Entities;
using MonoGame.Extended.TextureAtlases;

namespace Match3GameForest.Entities.Systems
{
    [Aspect(AspectType.All, typeof(ExplosionComponent), typeof(SpriteComponent), typeof(TransformComponent))]
    [EntitySystem(GameLoopType.Update, Layer = 0)]
    public class ExplosionSystem : EntityProcessingSystem
    {
        private SpriteSheetAnimation animation;
        private TextureAtlas explosionAtlas;

        public override void LoadContent()
        {
            base.LoadContent();
            var explosionTexture = Game.Content.Load<Texture2D>(ContentPath.explosion_3);
            explosionAtlas = TextureAtlas.Create("Animations/explosion-atlas", explosionTexture, 256, 256);
            animation = new SpriteSheetAnimation("explosionAnimation", explosionAtlas.Regions.ToArray());
        }

        public override void OnEntityAdded(Entity entity)
        {
            var explosion = entity.Get<ExplosionComponent>();
            var sprite = entity.Get<SpriteComponent>();
            var transform = entity.Get<TransformComponent>();

            animation.FrameDuration = explosion.Duration / explosionAtlas.RegionCount;
            animation.OnCompleted = () => { explosion.Entity.Destroy(); };

            sprite.Texture = explosionAtlas.Texture;
            sprite.Origin = new Vector2(explosionAtlas[0].Width / 2f, explosionAtlas[0].Height / 2f);

            transform.Scale = new Vector2(2);
        }

        protected override void Process(GameTime gameTime, Entity entity)
        {
            var sprite = entity.Get<SpriteComponent>();

            var deltaSeconds = (float)gameTime.ElapsedGameTime.TotalSeconds;

            animation.Update(deltaSeconds);
            sprite.SourceRectangle = animation.CurrentFrame.Bounds;
        }
    }
}