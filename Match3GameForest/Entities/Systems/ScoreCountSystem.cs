﻿using Match3GameForest.Entities.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using MonoGame.Extended.BitmapFonts;
using MonoGame.Extended.Entities;

namespace Match3GameForest.Entities.Systems
{
    [Aspect(AspectType.All, typeof(Board), typeof(ScoreCountComponent))]
    [EntitySystem(GameLoopType.Draw, Layer = 0)]
    public class ScoreCountSystem : EntityProcessingSystem
    {
        private Camera2D camera;
        private SpriteBatch spriteBatch;
        private BitmapFont font;

        public override void Initialize()
        {
            base.Initialize();

            camera = Game.Services.GetService<Camera2D>();
            spriteBatch = Game.Services.GetService<SpriteBatch>();
        }

        public override void LoadContent()
        {
            base.LoadContent();
            font = Game.Content.Load<BitmapFont>(ContentPath.Fonts.score_font);
        }

        public override void OnEntityAdded(Entity entity)
        {
            base.OnEntityAdded(entity);
            var board = entity.Get<Board>();
            var scoreCounter = entity.Get<ScoreCountComponent>();
            board.OnCellValueChange += (cell, value, newValue) =>
            {
                if (value != null && newValue == null) {
                    scoreCounter.Score++;
                }
            };
        }

        protected override void Begin(GameTime gameTime)
        {
            var transformMatrix = camera.GetViewMatrix();

            spriteBatch.Begin(transformMatrix: transformMatrix);
        }

        protected override void Process(GameTime gameTime, Entity entity)
        {
            var scoreCounter = entity.Get<ScoreCountComponent>();
            var board = entity.Get<Board>();
            var scoreStr = $"Score : {scoreCounter.Score}";
            var scoreStrRectangle = font.GetStringRectangle(scoreStr);
            var scorePosition = new Vector2(board.Bounds.Center.X - (scoreStrRectangle.Width / 2), board.Bounds.Top - font.LineHeight - 10);
            spriteBatch.DrawString(font, scoreStr, scorePosition, Color.White);
        }

        protected override void End(GameTime gameTime)
        {
            spriteBatch.End();
        }
    }
}