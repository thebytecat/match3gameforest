﻿using System;
using Match3GameForest.Components;
using Match3GameForest.Entities.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended;
using MonoGame.Extended.Entities;

namespace Match3GameForest.Entities.Systems
{
    [Aspect(AspectType.All, typeof(Board), typeof(BoardInputComponent), typeof(PieceSwapComponent), typeof(PieceMatchingComponent))]
    [EntitySystem(GameLoopType.Update, Layer = 0)]
    public class BoardInputSystem : EntityProcessingSystem
    {
        private MouseState previousMouseState;
        private MouseState mouseState;
        private Camera2D camera;

        private Point? GetCell(Board board, Point2 position)
        {
            if (!board.Bounds.Contains(position)) {
                return null;
            }

            var inBoardPosition = position - board.Bounds.TopLeft;
            var cell = (inBoardPosition / board.CellSize).ToPoint();

            return cell;
        }

        private bool IsNeighbor(Point A, Point B)
        {
            var dx = Math.Abs(A.X - B.X);
            var dy = Math.Abs(A.Y - B.Y);
            return (dx == 1 && dy == 0) || (dx == 0 && dy == 1);
        }

        public override void LoadContent()
        {
            base.LoadContent();
            camera = Game.Services.GetService<Camera2D>();
        }

        protected override void Begin(GameTime gameTime)
        {
            mouseState = Mouse.GetState();
        }

        protected override void Process(GameTime gameTime, Entity entity)
        {
            var board = entity.Get<Board>();
            var boardInput = entity.Get<BoardInputComponent>();

            if (board.State != BoardState.Playing) {
                return;
            }

            if (previousMouseState.LeftButton == ButtonState.Pressed && mouseState.LeftButton == ButtonState.Released) {

                var worldPosition = camera.ScreenToWorld(mouseState.Position.ToVector2());
                var cell = GetCell(board, worldPosition);

                if (cell == null)
                    return;

                var piece = board.GetPiece(cell.Value);

                if (boardInput.SelectedCell != null) {
                    var selectedPiece = board.GetPiece(boardInput.SelectedCell.Value);
                    if (boardInput.SelectedCell == cell) {
                        boardInput.SelectedCell = null;
                        selectedPiece.IsSelected = false;
                    } else {
                        if (board.GetPiece(cell.Value) != null && IsNeighbor(cell.Value, boardInput.SelectedCell.Value)) {
                            var matching = entity.Get<PieceMatchingComponent>();
                            var swap = entity.Get<PieceSwapComponent>();
                            if (matching.CheckSwap(cell.Value, boardInput.SelectedCell.Value)) {
                                swap.Swap(cell.Value, boardInput.SelectedCell.Value, board);
                            }
                            else {
                                swap.PlayNotAvailableSwapAnimation(cell.Value, boardInput.SelectedCell.Value, board);
                            }
                        }
                        boardInput.SelectedCell = null;
                        selectedPiece.IsSelected = false;
                    }
                } else {
                    if (board.GetPiece(cell.Value) != null) {
                        boardInput.SelectedCell = cell;
                        piece.IsSelected = true;
                    }
                }

            }

#if DEBUG
            if (previousMouseState.RightButton == ButtonState.Pressed && mouseState.RightButton == ButtonState.Released) {
                var worldPosition = camera.ScreenToWorld(mouseState.Position.ToVector2());
                var cell = GetCell(board, worldPosition);

                if (cell != null) {
                    var piece = board.GetPiece(cell.Value);
                    if (piece != null) {
                        board[cell.Value.X, cell.Value.Y] = null;
                    }
                }
            }
#endif
            previousMouseState = mouseState;
        }
    }
}