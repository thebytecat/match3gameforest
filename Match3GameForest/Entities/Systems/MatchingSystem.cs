﻿using System;
using System.Collections.Generic;
using System.Linq;
using Match3GameForest.Components;
using Match3GameForest.Entities.Components;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Collections;
using MonoGame.Extended.Entities;

namespace Match3GameForest.Entities.Systems
{
    [Aspect(AspectType.All, typeof(Board), typeof(PieceMatchingComponent), typeof(BonusMatchingComponent))]
    [EntitySystem(GameLoopType.Update, Layer = 0)]
    public class MatchingSystem : EntityProcessingSystem
    {

        public override void OnEntityAdded(Entity entity)
        {
            var board = entity.Get<Board>();
            var matching = entity.Get<PieceMatchingComponent>();

            matching.Board = board;
            board.OnCellValueChange += matching.CellValueChangeHandler;
        }

        protected override void Process(GameTime gameTime, Entity entity)
        {
            var board = entity.Get<Board>();
            var matching = entity.Get<PieceMatchingComponent>();
            var bonusMatching = entity.Get<BonusMatchingComponent>();

            if (board.State != BoardState.Playing || matching.ChangedCells.Count == 0)
                return;

            var matches = matching.GetAllMatches();

            bonusMatching.HandleMatch(matches, board);

            foreach (var match in matches) {
                foreach (var matchedCell in match.MatchedCells) {
                    board[matchedCell.X, matchedCell.Y] = null;
                }
            }

            matching.ChangedCells.Clear();
        }
    }
}