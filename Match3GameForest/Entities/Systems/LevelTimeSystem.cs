﻿using System;
using Match3GameForest.Entities.Components;
using Match3GameForest.Screens;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Entities;
using MonoGame.Extended.Screens;

namespace Match3GameForest.Entities.Systems
{
    [Aspect(AspectType.All, typeof(Board), typeof(LevelTimeComponent))]
    [EntitySystem(GameLoopType.Draw, Layer = 0)]
    public class LevelTimeSystem : EntityProcessingSystem
    {
        public override void OnEntityAdded(Entity entity)
        {
            var levelTime = entity.Get<LevelTimeComponent>();
            levelTime.TimeLeft = TimeSpan.FromSeconds(levelTime.TimeToLevel);
        }

        protected override void Process(GameTime gameTime, Entity entity)
        {
            var levelTime = entity.Get<LevelTimeComponent>();

            if (levelTime.TimeLeft <= TimeSpan.Zero) {
                Game.Services.GetService<ScreenGameComponent>().FindScreen<GameScreen>().GameOver();
            }

            levelTime.TimeLeft -= gameTime.ElapsedGameTime;
        }
    }
}