﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using MonoGame.Extended.Entities;
using SpriteComponent = Match3GameForest.Entities.Components.SpriteComponent;
using TransformComponent = Match3GameForest.Entities.Components.TransformComponent;

namespace Match3GameForest.Entities.Systems
{
    [Aspect(AspectType.All, typeof(SpriteComponent), typeof(TransformComponent))]
    [EntitySystem(GameLoopType.Draw, Layer = 0)]
    public class SpriteSystem : EntityProcessingSystem
    {
        private Camera2D camera;
        private SpriteBatch spriteBatch;
        private Texture2D pixelTexture;

        public SpriteSortMode SortMode { get; set; } = SpriteSortMode.Deferred;
        public BlendState BlendState { get; set; }
        public SamplerState SamplerState { get; set; }
        public DepthStencilState DepthStencilState { get; set; }
        public RasterizerState RasterizerState { get; set; }
        public Effect Effect { get; set; }

        public override void Initialize()
        {
            base.Initialize();

            camera = Game.Services.GetService<Camera2D>();
            spriteBatch = Game.Services.GetService<SpriteBatch>();
        }

        public override void LoadContent()
        {
            base.LoadContent();

            pixelTexture = new Texture2D(GraphicsDevice, 1, 1);
            pixelTexture.SetData(new [] { Color.White });
        }

        public override void UnloadContent()
        {
            pixelTexture.Dispose();

            base.UnloadContent();
        }

        protected override void Begin(GameTime gameTime)
        {
            var transformMatrix = camera.GetViewMatrix();

            spriteBatch.Begin(SortMode, BlendState, SamplerState, DepthStencilState, RasterizerState, Effect, transformMatrix);
        }

        protected override void Process(GameTime gameTime, Entity entity)
        {
            var sprite = entity.Get<SpriteComponent>();
            if (sprite.Texture == null)
                return;
            var transform = entity.Get<TransformComponent>();

            spriteBatch.Draw(sprite.Texture, transform.WorldPosition, sprite.SourceRectangle, sprite.Color,
                transform.WorldRotation, sprite.Origin, transform.WorldScale, sprite.Effects, sprite.Depth);
        }

        protected override void End(GameTime gameTime)
        {
            spriteBatch.End();
        }
    }
}