﻿using System;
using System.Collections.Generic;
using System.Linq;
using Match3GameForest.Components;
using Match3GameForest.Entities.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using MonoGame.Extended.Entities;
using MonoGame.Extended.Tweening;

namespace Match3GameForest.Entities.Systems
{
    [Aspect(AspectType.All, typeof(Board), typeof(PieceMatchingComponent), typeof(BonusMatchingComponent))]
    [EntitySystem(GameLoopType.Update, Layer = 1)]
    public class BonusMatchingSystem : EntityProcessingSystem
    {
        private EntityFactory entityFactory;
        private float destroyerSpeed = 0.1f;
        private float explosionDuration = 0.25f;
        private Texture2D destroyerSprite;

        public override void LoadContent()
        {
            base.LoadContent();
            destroyerSprite = Game.Content.Load<Texture2D>(ContentPath.arrow);

        }

        public override void OnEntityAdded(Entity entity)
        {
            var bonusMatching = entity.Get<BonusMatchingComponent>();

            var ecs = Game.Services.GetService<EntityComponentSystem>();
            entityFactory = new EntityFactory(ecs, Game.Content);
        }

        private void CreateDestroyer(Point cell, Point direction, Board board)
        {
            Point endCell = Point.Zero;
            if (direction.X > 0) {
                endCell = new Point(board.BoardSize.X - 1, cell.Y);
            } else if (direction.X < 0) {
                endCell = new Point(0, cell.Y);
            } else if (direction.Y > 0) {
                endCell = new Point(cell.X, board.BoardSize.Y - 1);
            } else if (direction.Y < 0) {
                endCell = new Point(cell.X, 0);
            }

            if(cell == endCell)
                return;

            var destroyer = entityFactory.CreateDestroyer();
            
            var sprite = destroyer.Get<SpriteComponent>();
            sprite.Texture = destroyerSprite;
            sprite.Origin = new Vector2(destroyerSprite.Width / 2f, destroyerSprite.Height / 2f);
            var transform = destroyer.Get<TransformComponent>();
            transform.Position = board.CellToWorldPosition(cell);
            transform.Rotation = direction.ToVector2().ToAngle();
            transform.Scale = new Vector2((float)board.CellSize / destroyerSprite.Width, (float)board.CellSize / destroyerSprite.Height);

            transform.CreateTweenGroup(() =>
            {
                destroyer.Destroy();
            }).MoveTo(board.CellToWorldPosition(endCell),
                destroyerSpeed * Math.Abs((endCell - cell).ToVector2().Length()), EasingFunctions.Linear);
        }

        private void DestroyCellByLineBonus(Point bonusCell, Point targetCell, Board board, BonusMatchingComponent bonusMatching)
        {
            if (bonusCell == targetCell)
                return;
            if (board[targetCell.X, targetCell.Y] != null) {
                bonusMatching.ActiveBonuses++;

                this.CreateTweenChain(() => {
                    if (board[targetCell.X, targetCell.Y] != null) {
                        var bonusType = board[targetCell.X, targetCell.Y].Bonus;
                        if (bonusType != BonusType.None) {
                            bonusMatching.MatchedBonuses.Add(
                                new Tuple<Point, BonusType>(targetCell, bonusType));
                        }

                        board[targetCell.X, targetCell.Y] = null;
                    }
                    bonusMatching.ActiveBonuses--;
                }).Delay(destroyerSpeed * (bonusCell - targetCell).ToVector2().Length());
            }
        }

        private List<Point> GetCellsInRadius(Point center, int radius, Board board)
        {
            var cells = new List<Point>();
            for (int x = Math.Max(center.X - radius, 0); x <= Math.Min(center.X + radius, board.BoardSize.X - 1); x++) {
                for (int y = Math.Max(center.Y - radius, 0); y <= Math.Min(center.Y + radius, board.BoardSize.Y - 1); y++) {
                    if (board[x, y] != null) {
                        cells.Add(new Point(x, y));
                    }
                }
            }

            return cells;
        } 

        private void ApplyBonus(Point cell, BonusType type, Board board, BonusMatchingComponent bonusMatching)
        {
            if (type != BonusType.None) {
                board.State = BoardState.Matching;
            }

            switch (type) {
                case BonusType.None:
                    break;
                case BonusType.HorizontalLine:
                    CreateDestroyer(cell, new Point(1, 0), board);
                    CreateDestroyer(cell, new Point(-1, 0), board);

                    for (int i = 0; i < board.BoardSize.X; i++) {
                        var targetCell = new Point(i, cell.Y);
                        DestroyCellByLineBonus(cell, targetCell, board, bonusMatching);
                    }

                    break;
                case BonusType.VerticalLine:
                    CreateDestroyer(cell, new Point(0, 1), board);
                    CreateDestroyer(cell, new Point(0, -1), board);

                    for (int i = 0; i < board.BoardSize.Y; i++) {
                        var targetCell = new Point(cell.X, i);
                        DestroyCellByLineBonus(cell, targetCell, board, bonusMatching);
                    }

                    break;
                case BonusType.Bomb:
                    bonusMatching.ActiveBonuses++;

                    entityFactory.CreateExplotion(explosionDuration, board.CellToWorldPosition(cell), board.CellSize);

                    var cells = GetCellsInRadius(cell, 1, board);

                    this.CreateTweenChain(() => {
                        foreach (var c in cells) {
                            var piece = board.GetPiece(c);
                            if (piece == null)
                                continue;

                            if (piece.Bonus != BonusType.None) {
                                bonusMatching.MatchedBonuses.Add(new Tuple<Point, BonusType>(c, piece.Bonus));
                            }
                            board[c.X, c.Y] = null;
                        }
                        bonusMatching.ActiveBonuses--;
                    }).Delay(explosionDuration);
                    break;
            }
        }

        private void CreateBonuses(List<Match> matches, Board board)
        {
            foreach (var match in matches) {
                if (match.MatchedCells.Count <= 3) continue;

                BonusType bonusType;
                if (match.MatchedCells.Count == 4) {
                    bonusType = Math.Abs(match.MatchedCells[0].X - match.MatchedCells[1].X) > 0
                        ? BonusType.VerticalLine
                        : BonusType.HorizontalLine;
                }
                else{
                    bonusType = BonusType.Bomb;
                }
                    
                var piece = entityFactory.CreatePiece(board, match.CoreCell).Get<BoardPiece>();
                piece.Bonus = bonusType;
                piece.Type = match.Type;
                board[match.CoreCell.X, match.CoreCell.Y] = piece;
            }
        }

        protected override void Process(GameTime gameTime, Entity entity)
        {
            var board = entity.Get<Board>();
            var bonusMatching = entity.Get<BonusMatchingComponent>();

            if (board.State == BoardState.Matching && bonusMatching.ActiveBonuses == 0) {
                board.State = BoardState.Playing;
            }

            if (bonusMatching.MatchedBonuses.Count > 0) {
                foreach (var bonus in bonusMatching.MatchedBonuses) {
                    ApplyBonus(bonus.Item1, bonus.Item2, board, bonusMatching);
                }

                bonusMatching.MatchedBonuses.Clear();
            }

            if (bonusMatching.Matches != null) {
                CreateBonuses(bonusMatching.Matches, board);
                bonusMatching.Matches = null;
            }
        }
    }
}