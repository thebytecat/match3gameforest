﻿using System.Collections.Generic;
using Match3GameForest.Components;
using Match3GameForest.Entities.Components;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Entities;
using MonoGame.Extended.Tweening;

namespace Match3GameForest.Entities.Systems
{
    [Aspect(AspectType.All, typeof(Board), typeof(BoardFillComponent))]
    [EntitySystem(GameLoopType.Update, Layer = 2)]
    public class BoardFillSystem : EntityProcessingSystem
    {
        private readonly Dictionary<int, int> emptyInColumn = new Dictionary<int, int>();
        private EntityFactory entityFactory;

        private void MovePieceDown(BoardPiece piece, Point to, int height, Board board, BoardFillComponent boardFill)
        {
            var transform = piece.Entity.Get<TransformComponent>();
            boardFill.FallingCellCount++;
            transform.CreateTweenGroup(() => {
                    boardFill.FallingCellCount--;
                    board[to.X, to.Y] = piece;
                })
                .MoveTo(board.CellToWorldPosition(to), boardFill.FallingSpeed * height, EasingFunctions.Linear);
        }

        public override void LoadContent()
        {
            base.LoadContent();
            var ecs = Game.Services.GetService<EntityComponentSystem>();
            entityFactory = new EntityFactory(ecs, Game.Content);
        }

        public override void OnEntityAdded(Entity entity)
        {
            var board = entity.Get<Board>();
            var boardFill = entity.Get<BoardFillComponent>();

            boardFill.EmptyCellInColumn = new int[board.BoardSize.X];
            board.OnCellValueChange += boardFill.CellValueChangeHandler;
        }

        protected override void Process(GameTime gameTime, Entity entity)
        {
            var board = entity.Get<Board>();
            var boardFill = entity.Get<BoardFillComponent>();

            if (boardFill.FallingCellCount > 0) {
                return;
            }

            if (!boardFill.HaveEmptyCell) {
                if (board.State == BoardState.Falling) {
                    board.State = BoardState.Playing;
                }
                return;
            }

            if (board.State != BoardState.Falling && board.State != BoardState.Playing)
                return;

            board.State = BoardState.Falling;

            emptyInColumn.Clear();
            for (int x = 0; x < board.BoardSize.X; x++) {
                if (boardFill.EmptyCellInColumn[x] == 0)
                    continue;

                emptyInColumn.Add(x, 0);

                for (int y = board.BoardSize.Y - 1; y >= 0; y--) {
                    if (board[x, y] == null) {
                        emptyInColumn[x]++;
                    } else if (emptyInColumn[x] > 0) {
                        
                        var piece = board[x, y];
                        var to = new Point(x, y + emptyInColumn[x]);
                        MovePieceDown(piece, to, emptyInColumn[x], board, boardFill);
                    }
                }

                for (int y = 0; y < emptyInColumn[x]; y++) {
                    var piece = entityFactory.CreatePiece(board, x, -y - 1).Get<BoardPiece>();
                    var to = new Point(x, emptyInColumn[x] - y - 1);
                    MovePieceDown(piece, to, emptyInColumn[x], board, boardFill);
                }

                boardFill.EmptyCellInColumn[x] = 0;
            }
        }
    }
}