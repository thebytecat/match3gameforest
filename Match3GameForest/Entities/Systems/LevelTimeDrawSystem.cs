﻿using Match3GameForest.Entities.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using MonoGame.Extended.BitmapFonts;
using MonoGame.Extended.Entities;

namespace Match3GameForest.Entities.Systems
{
    [Aspect(AspectType.All, typeof(Board), typeof(LevelTimeComponent))]
    [EntitySystem(GameLoopType.Draw, Layer = 0)]
    public class LevelTimeDrawSystem :EntityProcessingSystem
    {
        private Camera2D camera;
        private SpriteBatch spriteBatch;
        private BitmapFont font;

        public override void Initialize()
        {
            base.Initialize();

            camera = Game.Services.GetService<Camera2D>();
            spriteBatch = Game.Services.GetService<SpriteBatch>();
        }

        public override void LoadContent()
        {
            base.LoadContent();
            font = Game.Content.Load<BitmapFont>(ContentPath.Fonts.score_font);
        }

        protected override void Begin(GameTime gameTime)
        {
            var transformMatrix = camera.GetViewMatrix();

            spriteBatch.Begin(transformMatrix: transformMatrix);
        }

        protected override void Process(GameTime gameTime, Entity entity)
        {
            var levelTime = entity.Get<LevelTimeComponent>();
            var board = entity.Get<Board>();
            var timeStr = $"Time Left {levelTime.TimeLeft.Minutes}:{levelTime.TimeLeft.Seconds:00}";
            var timeStrRectangle = font.GetStringRectangle(timeStr);
            var scorePosition = new Vector2(board.Bounds.Center.X - (timeStrRectangle.Width / 2), board.Bounds.Bottom + 10);
            spriteBatch.DrawString(font, timeStr, scorePosition, Color.DarkSlateGray);
        }

        protected override void End(GameTime gameTime)
        {
            spriteBatch.End();
        }
    }
}